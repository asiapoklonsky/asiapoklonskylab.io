#!/bin/bash
DIR=$1
cd $DIR
for i in  *.png ; do
  j=$(echo -n $i | sed -e 's/.png/.jpg/g')
  echo "--converting $i to $j"
  sips -s format jpeg "$i" --out "$j"
done
